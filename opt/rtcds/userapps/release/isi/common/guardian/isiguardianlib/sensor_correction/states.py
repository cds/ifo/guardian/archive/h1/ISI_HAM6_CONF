# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$

from guardian import GuardState
from ezca.ligofilter import LIGOFilter, LIGOFilterManager
from SENSCOR import LIGOSensorCorrectionManager, LIGOSensorCorrectionError
from .. import const as top_const
import const
import util
import time
          
####################
# States

def get_SC_filter_and_BRS_switching(SC_banks, dof_list, filter_on_list, BRS_arg, no_BRS_coord, BRS_coord, nodes, wait_settle=False, requestable=False):
    class SC_filter_and_BRS_switch(GuardState):
        """Changes both the BRS and the SC filters.
        
        Possible arguments:
        
        SC_banks - A list of banks that you desire to make a change in.
        
        dof_list - This is a list of degrees of freedom in the specified bank that will be changed.
        
        filter_on_list - List of filters that should be turned NO. Ex: [1,3,6]
        
        BRS_arg - Either 'ON' or 'OFF' to turn the BRS SC on or off.

        no_BRS_coord - A tuple of matrix element coordinates where, if the element is set to 1.0, 
                it would not be in a configuration to use BRS SC. Ex: (1,4)
                This matrix element will also be turned off to use BRS SC.

        BRS_coord - A tuple of matrix element coordinates where, if the element is set to 1.0, 
                it would be in a configuration to use BRS SC. Ex: (1,7)
                This element should be turned off when not using BRS SC.

        nodes - NodeManager object

        wait_settle - Custom time to wait for the output to settle.
        """
        request = requestable
        @nodes.checker()
        def main(self):
            # Instantiate
            self.scm = LIGOSensorCorrectionManager(SC_banks=SC_banks, deg_of_free_list=dof_list, ezca=ezca)
            # check that the filters are not already changing
            for bank in SC_banks:
                for dof in dof_list:
                    if util.is_switching_filters(bank, dof):
                        # wait for filters to finish switching
                        time.sleep(1)
                        # if it's still ramping gain, raise an error
                        if util.is_switching_filters(SC_banks, dof_list):
                            raise LIGOSensorCorrectionError("%s SC is still ramping gains after 30sec" %(top_const.CHAMBER))
            if wait_settle:
                self.scm.switch_sensor_correction(filter_on_list, wait=wait_settle)
            else:
                self.scm.switch_sensor_correction(filter_on_list)
            self.scm.BRS_SC_switch(BRS_arg, no_BRS_coord, BRS_coord)

        @nodes.checker()
        def run(self):
            if wait_settle == False:
                self.scm.gain_back_up()
            # make sure that it is done switching
            for deg_of_free in const.SC_DOF_LIST:
                if self.scm.is_switching_filters():
                    return False
            return True

    return SC_filter_and_BRS_switch


def get_SC_filter_switching(SC_banks, dof_list, filter_on_list, nodes, wait_settle=False, requestable=False):
    class SC_filter_switch_only(GuardState):
        """This is the basic state to switch Sensor Correction filters.
        
        Possible arguments:
        
        SC_banks - A list of banks that you desire to make a change in.
        
        dof_list - This is a list of degrees of freedom in the specified bank that will be changed.
        
        filter_on_list - List of filters that should be turned NO. Ex: [1,3,6]

        nodes - NodeManager object

        wait_settle - Custom time to wait for the output to settle.
        """
        request = requestable
        def main(self):
            # Instantiate
            self.scm = LIGOSensorCorrectionManager(SC_banks=SC_banks, deg_of_free_list=dof_list, ezca=ezca)
            # check that the filters are not already changing
            for bank in SC_banks:
                for dof in dof_list:
                    if util.is_switching_filters(bank, dof):
                        # wait for filters to finish switching
                        time.sleep(1)
                        # if it's still ramping gain, raise an error
                        if util.is_switching_filters(SC_banks, dof_list):
                            raise LIGOSensorCorrectionError("%s SC is still ramping gains after 30sec" %(top_const.CHAMBER))
            if wait_settle:
                self.scm.switch_sensor_correction(filter_on_list, wait=wait_settle)
            else:
                self.scm.switch_sensor_correction(filter_on_list)
            
        @nodes.checker()
        def run(self):
            if wait_settle == False:
                self.scm.gain_back_up()
            # make sure that it is done switching
            for deg_of_free in const.SC_DOF_LIST:
                if self.scm.is_switching_filters():
                    return False
            return True
            
    return SC_filter_switch_only


def get_BRS_SC_switching(BRS_arg, no_BRS_coord, BRS_coord, nodes, requestable=False):
    class BRS_SC_switch(GuardState):
        """State that switches the BRS Sensor Correction only.
        
        Arguments:

        BRS_arg -'ON' - will use the BRS sensor corrected signal by changing matrix values in STS 2CART.
                 'OFF' - will not use the BRS signal.

        no_BRS_coord - A tuple of matrix element coordinates where, if the element is set to 1.0, 
                it would not be in a configuration to use BRS SC. Ex: (1,4)
                This matrix element will also be turned off to use BRS SC.

        BRS_coord - A tuple of matrix element coordinates where, if the element is set to 1.0, 
                it would be in a configuration to use BRS SC. Ex: (1,7)
                This element should be turned off when not using BRS SC.

        nodes - NodeManager object
        
        Careful here, make sure that the rest of the system is in the configuration that
        it needs to be in (whatever that may be) before calling on this state.
        """
        request = requestable
        @nodes.checker()
        def main(self):
            # instantiate
            self.scm = LIGOSensorCorrectionManager(const.SC_FILTER_BANK_NAMES_ALL, const.SC_DOF_LIST, ezca)
            # make sure that the filters are not ramping gains trying to switch
            if util.is_switching_filters(SC_banks, dof_list):
                # Give them time to finish
                time.sleep(2)
                # if it's still ramping gain, raise an error
                if util.is_switching_filters(SC_banks, dof_list):
                    raise LIGOSensorCorrectionError("%s SC is still ramping gains after 30sec" %(top_const.CHAMBER))
            # switch BRS sensor correction
            self.scm.BRS_SC_switch(BRS_arg, no_BRS_coord, BRS_coord)
        @nodes.checker()
        def run(self):
            return True
    
    return BRS_SC_switch


########################################
# The below states are for a configuration that will only ramp between 
# two diffferent SC filter bank gains. This assumes that the correct filters
# are already installed in the correct places and selected.


def get_SC_bank_gain_switch(old_bank, old_dof, new_bank, new_dof, ramp_time=const.DEFAULT_RAMP_TIME, requestable=False):
    class SC_bank_gain_switch(GuardState):
        """Act like a switch and turn the gain off in the old bank, and on in the new.

        """
        request = requestable
        def main(self):
            old_bank_chan = const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=old_dof, bank=old_bank)
            new_bank_chan = const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=new_dof, bank=new_bank)
            # Ramp OFF the gain of the old bank
            lf_old_bank = LIGOFilter(old_bank_chan, ezca)
            log('Ramping OFF the {} {} bank'.format(old_bank, old_dof))
            lf_old_bank.ramp_gain(0, ramp_time=ramp_time, wait=True)
            # Ramp ON the gain of the new bank
            lf_new_bank - LIGOFilter(new_bank_chan, ezca)
            log('Ramping ON the {} {} bank'.format(new_bank, new_dof))
            lf_new_bank.ramp_gain(1, ramp_time=ramp_time, wait=True)
            return True
    return SC_bank_gain_switch

def get_SC_switch_one_bank(bank, dofs, gain, ramp_time=const.DEFAULT_RAMP_TIME, requestable=False):
    class SC_switch_one_bank(GuardState):
        """Ramp ON the gain of the bank and its dof(s) before the MATCH bank
        and then ramp ON the MATCH bank.
        """
        request = requestable
        def main(self):
            for dof in dofs:
                # Ramp ON the filter gain
                lf_bank = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank=bank), ezca)
                lf_bank.ramp_gain(gain, ramp_time=ramp_time, wait=True)
                # Ramp ON the MATCH bank to allow the previous filter signal through
                lf_match = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof, bank='MATCH'), ezca)
                lf_match.ramp_gain(gain, ramp_time=ramp_time, wait=True)
            return True
    return SC_switch_one_bank

def get_SC_all_gains_off(bank_list, banks_dofs, ramp_time=const.DEFAULT_RAMP_TIME, requestable=False):
    class SC_all_gains_off(GuardState):
        """Turn OFF the gain in all of the banks listed in the degree of freedom
        specified. Ramp time can be set manually or as teh default.
        """
        request = requestable
        def main(self):
            for dof in banks_dofs:
                lfm = LIGOFilterManager([const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof,bank=bank) for bank in bank_list], ezca)
                def gains_off(ligo_filter):
                        ligo_filter.ramp_gain(0, ramp_time=ramp_time, wait=True)
                lfm.all_do(gains_off)
            return True
    return SC_all_gains_off
